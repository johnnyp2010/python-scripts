import pyttsx3, PyPDF2, re

pdf_path = "test.pdf"
pdf_reader = PyPDF2.PdfReader(open(pdf_path, 'rb'))
speaker = pyttsx3.init()

for page_number in range(len(pdf_reader.pages)):
    page = pdf_reader.pages[page_number]
    text = page.extract_text(0)
    # clean_text = text.strip().replace('\n', ' ')
    clean_text = re.sub(".\s.", " ", text)
    print(clean_text)

speaker.save_to_file(clean_text, 'test.mp3')
speaker.runAndWait()

speaker.stop()