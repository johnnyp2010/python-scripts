import pandas_datareader as data
import pandas as pd
data_source = 'yahoo'
start_date = '2016-01-01'
end_date = '2021-11-30'
Google = data.DataReader('GOOG', data_source, start_date, end_date)
Amazon = data.DataReader('AMZN', data_source, start_date, end_date)
Microsoft = data.DataReader('MSFT', data_source, start_date, end_date)
Apple = data.DataReader('AAPL', data_source, start_date, end_date)
Facebook = data.DataReader('FB', data_source, start_date, end_date)
df = pd.concat([Google, Amazon, Microsoft, Apple, Facebook], axis=1)
print(df.head(3))