import numpy as np

# Slicing arrays
np1 = np.array([1,2,3,4,5,6,7,8,9])

# Return 2,3,4,5
print(np1[1:5])

# Return from something to the end
print(np1[3:])

#Return negative slice
print(np1[-3:-1])

# Steps
print(np1[1:5])
print(np1[1:5:2])

# Steps on the entire array
print(np1[::2])

# Slice a 2d array
np2 = np.array([[1,2,3,4,5], [6,7,8,9,10]])
# Pull out a single item
print(np2[1,2])

# Slice array
print(np2[0:1, 1:3])

#Slice from both rows
print(np2[0:2, 1:3])