from pytube import YouTube 
from sys import argv

url = argv[1]
video = YouTube(url)

print(f"Downloading video: {video.title}")
stream = video.streams.get_highest_resolution()
stream.download("/home/johnny/Desktop")