from google_service import Create_Service
from googleapiclient.http import MediaFileUpload


client_secret_file = 'credentials.json'
api_name = 'drive'
api_version = 'v3'
scopes = ['https://www.googleapis.com/auth/drive']

service = Create_Service(client_secret_file, api_name, api_version, scopes)

folder_id = '1qEU_HNGhNNWzPfqIi1ZQW24ziXyfqNjh'
file_name = 'valorant_stats 2022-12-30 10:21:35.xlsx'
mime_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

file_metadata = {
    'name': file_name,
    'parents': [folder_id]
}

media = MediaFileUpload(
    f'/home/johnny/Desktop/{file_name}', mimetype=mime_type)

service.files().create(
    body=file_metadata,
    media_body=media,
    fields='id'
).execute()
