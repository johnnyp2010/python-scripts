import yfinance as yf
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
plt.style.use('fivethirtyeight')

msft = yf.Ticker('msft')

stockinfo = msft.info

# for key,value in stockinfo.items():
#     print(key, ":", value)

# numshares = msft.info['sharesOutstanding']
# print(numshares)

df = msft.dividends

data = df.resample('Y').sum()

data = data.reset_index()

data['Year'] = data['Date'].dt.year

plt.figure()
plt.bar(data['Year'], data['Dividends'])
plt.ylabel('Dividend Yield ($)')
plt.xlabel('Year')
plt.show()